'use strict';

var should = require('should'),
  http_mocks = require('node-mocks-http'),
  ObjectId = require('mongoose').Types.ObjectId,
  mockery = require('mockery');

describe('Message API Controller Test Case', function () {
  before(function () {
    mockery.enable({
      warnOnUnregistered: false,
      warnOnReplace: false
    });

    mockery.registerMock('striptags', function (row) {
      return row;
    });

    mockery.registerMock('../../models/message', {
      paginate: function (x, y, end) {
        end(null, {
          pages: 0,
          total: 0,
          docs: []
        });
      },

      create: function (message, end) {
        if (message.content === undefined) {
          return end({'error': 'Invalid Parameters'}, null);
        }

        end(null, {});
      }
    });

    this.controller = require('../../../routes/api/message');
  });

  after(function () {
    mockery.disable()
  });

  it('Message API GET with channel must be result in list object', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'GET',
      url: '/1',
      params: {
        channel: 1
      },
      session: {
        user: {
          id: 1
        }
      }
    });

    this.controller.handle(request, response);

    var data = JSON.parse(response._getData());

    should.equal(response.statusCode, 200);
    should.equal(response.statusMessage, 'OK');
    should.equal(data.object, 'list');
    should.equal(data.itemCount, 0);
    should.equal(data.pageCount, 0);

    done();
  });

  it('Message API POST with parameters must be result created message', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'POST',
      body: {
        content: 'foo'
      },
      url: '/1',
      params: {
        channel: 1
      },
      session: {
        user: {
          _id: new ObjectId()
        }
      }
    });

    this.controller.handle(request, response);

    var data = JSON.parse(response._getData());

    should.equal(response.statusCode, 201);
    should.equal(response.statusMessage, 'OK');
    should.equal(data.object, 'object');
    should.equal(data.itemCount, 1);
    should.equal(data.pageCount, 1);

    done();
  });

  it('Message API POST without parameters must be result exception', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'POST',
      body: {},
      url: '/1',
      params: {
        channel: 1
      },
      session: {
        user: {
          _id: 1
        }
      }
    });

    this.controller.handle(request, response);

    var data = JSON.parse(response._getData());

    should.equal(response.statusCode, 500);
    should.equal(response.statusMessage, 'OK');
    should.equal(data.object, 'error');
    should.equal(data.itemCount, 0);
    should.equal(data.pageCount, 0);

    done();
  });
});

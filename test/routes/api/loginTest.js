'use strict';

var should = require('should'),
  http_mocks = require('node-mocks-http'),
  mockery = require('mockery');

describe('Login API Controller Test Case', function () {
  before(function () {
    process.env.PASSWORD_SALT = 'foo';

    mockery.enable({
      warnOnUnregistered: false,
      warnOnReplace: false
    });

    mockery.registerMock('striptags', function (row) {
      return row;
    });

    mockery.registerMock('bcrypt', {
      hashSync: function(x, y) {
        return '1234567890';
      }
    });

    mockery.registerMock('../../models/user', {
      findOne: function (filter) {
        if (filter.email === 'foo@bar.bar' && filter.password === '1234567890') {
          return {
            exec: function (end) {
              end(null, {
                _id: parseInt(Math.random()),
                email: filter.email
              });
            }
          }
        } else {
          return {
            exec: function (end) {
              end(null, null);
            }
          }
        }
      }
    });

    mockery.registerMock('../../models/token', {
      findOne: function (x) {
        return {
          populate: function () {
            return []
          }
        }
      },
      create: function (x, end) {
        end(null, {});
      }
    });

    this.controller = require('../../../routes/api/login');
  });

  after(function () {
    mockery.disable()
  });

  it('Login API POST with valid user must be result object', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'POST',
      url: '/',
      body: {
        email: 'foo@bar.bar',
        password: '1234567890'
      },
      session: {}
    });

    this.controller.handle(request, response, function () {
    });

    var data = JSON.parse(response._getData());

    should.equal(response.statusCode, 201);
    should.equal(response.statusMessage, 'OK');
    should.equal(data.object, 'object');
    should.equal(data.has_more, false);
    should.equal(data.itemCount, 1);
    should.equal(data.pageCount, 1);

    done();
  });

  it('Login API POST with invalid user must be result error', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'POST',
      url: '/',
      body: {
        email: 'foo@bar.br',
        password: '12347890'
      }
    });

    this.controller.handle(request, response, function () {
    });

    var data = JSON.parse(response._getData());

    should.equal(response.statusCode, 403);
    should.equal(response.statusMessage, 'OK');
    should.equal(data.object, 'object');
    should.equal(data.has_more, false);
    should.equal(data.itemCount, 0);
    should.equal(data.pageCount, 0);

    done();
  });
});

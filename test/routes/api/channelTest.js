'use strict';

var should = require('should'),
  http_mocks = require('node-mocks-http'),
  mockery = require('mockery');

describe('Channel API Controller Test Case', function () {
  before(function () {
    mockery.enable({
      warnOnUnregistered: false,
      warnOnReplace: false
    });

    mockery.registerMock('striptags', function (row) {
      return row;
    });

    mockery.registerMock('../../models/channel', {
      paginate: function (x, y, end) {
        end(null, {
          pages: 0,
          total: 0,
          docs: []
        });
      },

      create: function (channel, end) {
        if (channel.name === undefined) {
          return end({'error': 'Invalid Parameters'}, null);
        }

        end(null, {});
      }
    });

    this.controller = require('../../../routes/api/channel');
  });

  after(function () {
    mockery.disable()
  });

  it('Channel API GET without parameters must be result list of channels', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'GET',
      url: '/',
      session: {
        user: {
          id: 1
        }
      }
    });

    this.controller.handle(request, response, function () {
    });

    var data = JSON.parse(response._getData());

    should.equal(response.statusCode, 200);
    should.equal(response.statusMessage, 'OK');
    should.equal(data.object, 'list');
    should.equal(data.itemCount, 0);
    should.equal(data.pageCount, 0);

    done();
  });

  it('Channel API POST with parameters must be result created channel', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'POST',
      body: {
        name: 'foo'
      },
      url: '/',
      session: {
        user: {
          id: 1
        }
      }
    });

    this.controller.handle(request, response, function () {
    });

    var data = JSON.parse(response._getData());

    should.equal(response.statusCode, 201);
    should.equal(response.statusMessage, 'OK');
    should.equal(data.object, 'object');
    should.equal(data.itemCount, 1);
    should.equal(data.pageCount, 1);

    done();
  });

  it('Channel API POST without parameters must be result exception', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'POST',
      body: {},
      url: '/',
      session: {
        user: {
          _id: 1
        }
      }
    });

    this.controller.handle(request, response, function () {
    });

    var data = JSON.parse(response._getData());

    should.equal(response.statusCode, 500);
    should.equal(response.statusMessage, 'OK');
    should.equal(data.object, 'error');
    should.equal(data.itemCount, 0);
    should.equal(data.pageCount, 0);

    done();
  });
});

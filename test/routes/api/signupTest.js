'use strict';

var should = require('should'),
  http_mocks = require('node-mocks-http'),
  mockery = require('mockery');

describe('User API Controller Test Case', function () {
  before(function () {
    mockery.enable({
      warnOnUnregistered: false,
      warnOnReplace: false
    });

    mockery.registerMock('bcrypt', {
      hashSync: function () {
        return true;
      }
    });

    mockery.registerMock('striptags', function (row) {
      return row;
    });

    mockery.registerMock('../../models/user', {
      create: function (user, end) {
        if (user.name === undefined || user.email === undefined || user.password === undefined) {
          return end({'error': 'Invalid Parameters'}, null);
        }

        end(null, {});
      }
    });

    this.controller = require('../../../routes/api/signup');
  });

  after(function () {
    mockery.disable()
  });

  it('User API POST with parameters must be result created user', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'POST',
      body: {
        name: 'foo',
        email: 'foo@bar.bar',
        password: 'foo'
      },
      url: '/'
    });

    this.controller.handle(request, response, function () {
    });

    var data = JSON.parse(response._getData());

    should.equal(response.statusCode, 201);
    should.equal(response.statusMessage, 'OK');
    should.equal(data.object, 'object');
    should.equal(data.has_more, false);
    should.equal(data.itemCount, 1);
    should.equal(data.pageCount, 1);

    done();
  });

  it('User API POST without parameters must be result exception', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'POST',
      body: {},
      url: '/'
    });

    this.controller.handle(request, response, function () {
    });

    var data = JSON.parse(response._getData());

    should.equal(response.statusCode, 500);
    should.equal(response.statusMessage, 'OK');
    should.equal(data.object, 'error');
    should.equal(data.has_more, false);
    should.equal(data.itemCount, 0);
    should.equal(data.pageCount, 0);

    done();
  });
});

'use strict';

var should = require('should'),
  http_mocks = require('node-mocks-http'),
  mockery = require('mockery');

describe('Channel Controller Test Suite', function () {
  before(function () {
    mockery.enable({
      warnOnUnregistered: false,
      warnOnReplace: false
    });

    this.controller = require('../../../routes/ui/channel');
  });

  after(function () {
    mockery.disable()
  });

  it('Channels page without must be show user form when not logged.', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'GET',
      url: '/'
    });

    this.controller.handle(request, response);

    should.equal(response.statusCode, 200);
    should.equal(response.statusMessage, 'OK');

    done();
  });

  it('Channel page with id param must be show channel page', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'GET',
      url: '/1',
      params: {
        id: 1
      }
    });

    this.controller.handle(request, response);

    should.equal(response.statusCode, 200);
    should.equal(response.statusMessage, 'OK');

    done();
  });
});

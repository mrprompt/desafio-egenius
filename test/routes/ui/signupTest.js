'use strict';

var should = require('should'),
  http_mocks = require('node-mocks-http'),
  mockery = require('mockery');

describe('Signup Controller Test Suite', function () {
  before(function () {
    mockery.enable({
      warnOnUnregistered: false,
      warnOnReplace: false
    });

    this.controller = require('../../../routes/ui/signup');
  });

  after(function () {
    mockery.disable()
  });

  it('Signup page without must be show user form when not logged.', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'GET',
      url: '/'
    });

    this.controller.handle(request, response, function () {
    });

    should.equal(response.statusCode, 200);
    should.equal(response.statusMessage, 'OK');

    done();
  });

  it('Signup page without must be redirect to /channels when logged.', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'GET',
      url: '/',
      session: {
        user: {
          _id: 1
        }
      }
    });

    this.controller.handle(request, response, function () {
    });

    should.equal(response.statusCode, 302);
    should.equal(response.statusMessage, 'OK');

    done();
  });
});

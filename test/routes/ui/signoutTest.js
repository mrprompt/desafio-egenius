'use strict';

var should = require('should'),
  http_mocks = require('node-mocks-http'),
  mockery = require('mockery');

describe('Signout Controller Test Suite', function () {
  before(function () {
    mockery.enable({
      warnOnUnregistered: false,
      warnOnReplace: false
    });

    this.controller = require('../../../routes/ui/signout');
  });

  after(function () {
    mockery.disable()
  });

  it('Signout page without must be redirect to home when receive get', function (done) {
    var response = http_mocks.createResponse();

    var request = http_mocks.createRequest({
      method: 'GET',
      url: '/'
    });

    this.controller.handle(request, response, function () {
    });

    should.equal(response.statusCode, 200);
    should.equal(response.statusMessage, 'OK');

    done();
  });
});

'use strict';

var Channel = require('../../models/channel');
var should = require('should');

describe('Channel Model Test Case', function () {
  it('Channel model must be start without errors', function (done) {
    var channel = new Channel();

    should(channel.isNew).is.exactly(true);
    should(channel.name).is.exactly(undefined);
    should(channel.user).is.exactly(undefined);
    should(channel.created).is.a.Object();

    done();
  });
});

'use strict';

var Message = require('../../models/message');
var should = require('should');

describe('Message Model Test Case', function () {
  it('Message model must be start without errors', function (done) {
    var message = new Message();

    should(message.isNew).is.exactly(true);
    should(message.channel).is.exactly(undefined);
    should(message.user).is.exactly(undefined);
    should(message.content).is.exactly(undefined);
    should(message.created).is.a.Object();

    done();
  });
});

'use strict';

var User = require('../../models/user');
var should = require('should');

describe('User Model Test Case', function () {
  it('User model must be start without errors', function (done) {
    var user = new User();

    should(user.isNew).is.exactly(true);
    should(user.name).is.exactly(undefined);
    should(user.email).is.exactly(undefined);
    should(user.password).is.exactly(undefined);
    should(user.created).is.a.Object();

    done();
  });
});

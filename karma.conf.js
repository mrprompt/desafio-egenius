module.exports = function (config) {
    config.set({

        basePath: './',

        files: [
            'bower_components/angular/angular.js',
            'bower_components/angular-sanitize/angular-sanitize.js',
            'bower_components/angular-loader/angular-loader.js',
            'bower_components/angular-mocks/angular-mocks.js',
            'bower_components/angular-route/angular-route.js',
            'bower_components/angular-i18n/angular-locale_pt-br.js',
            'bower_components/angular-loading-bar/build/loading-bar.min.js',
            'bower_components/angular-flash-alert/dist/angular-flash.min.js',
            'bower_components/ng-notify/dist/ng-notify.min.js',
            'bower_components/angular-gravatar/build/angular-gravatar.min.js',
            'bower_components/jquery/dist/jquery.min.js',
            'bower_components/bootstrap/dist/js/bootstrap.min.js',
            'public/javascripts/**/*.js',
            'e2e-tests/javascripts/**/*.js'
        ],

        autoWatch: true,

        frameworks: ['jasmine'],

        browsers: ['PhantomJS'],

        plugins: [
            'karma-chrome-launcher',
            'karma-firefox-launcher',
            'karma-phantomjs-launcher',
            'karma-jasmine'
        ]
    });
};

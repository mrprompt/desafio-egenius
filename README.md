# Desafio eGenius

[![Build Status](https://travis-ci.org/mrprompt/desafio-egenius.svg?branch=master)](https://travis-ci.org/mrprompt/desafio-egenius)
[![Code Climate](https://codeclimate.com/github/mrprompt/desafio-egenius/badges/gpa.svg)](https://codeclimate.com/github/mrprompt/desafio-egenius)
[![Issue Count](https://codeclimate.com/github/mrprompt/desafio-egenius/badges/issue_count.svg)](https://codeclimate.com/github/mrprompt/desafio-egenius)
[![Codacy Badge](https://api.codacy.com/project/badge/Grade/9ad59fdc56c54ad58b70c3a1298d02ad)](https://www.codacy.com/app/mrprompt/desafio-egenius?utm_source=github.com&amp;utm_medium=referral&amp;utm_content=mrprompt/desafio-egenius&amp;utm_campaign=Badge_Grade)
[![Heroku](https://heroku-badge.herokuapp.com/?app=egenius)](https://egenius.herokuapp.com/)

Desafio FullStack eGenius

### Preparando o terreno

Antes de tudo, é necessário exportar as seguintes variáveis de ambiente

```
DB_USERNAME=admin
DB_PASSWORD=admin
DB_HOST=127.0.0.1
DB_PORT=27017
DB_NAME=test
PASSWORD_SALT=$2a$10$XN0gPXC7/pOCU2lJ9qic4u
NODE_ENV=development
```

### Para rodar

No MacOS ou Linux, execute o aplicativo com este comando:
```
DEBUG=* npm start
```

No Windows, use este comando:
```
set DEBUG=* & npm start
```


### Testando

```
npm test
npm run protractor
mocha
```

### Demo

Você pode visualizar o exemplo no endereço: 
[eGenius - Chat](https://egenius.herokuapp.com/)


### O Desafio

Imagine que um cliente peça para você desenvolver um sistema de chat, o sistema deverá ter as seguintes telas e 
funcionar conforme descrito abaixo:

- Uma tela para cadastrar usuários
- Uma tela para cadastrar novas salas de chat
- Um usuário logado poderá acessar as salas disponíveis para trocar mensagens
- Uma tela para listar as salas disponíveis
- Uma tela para login 

Ao acessar a sala devemos ver o histórico de mensagens que já foram trocadas na sala e o usuário que a enviou. 

Para esse desafio deverá ser utilizada a stack MEAN (Mongo, Express, Angular e NodeJS). 

Após concluído o código deverá ser publicado no Github ou Bitbucket. Utilizar socket será um diferencial. 
Poderá ser utilizado um banco de dados relacional ao invés do Mongo.

'use strict';

var express = require('express');
var router = express.Router();
var striptags = require('striptags');
var ChannelModel = require('../../models/channel');

router.get('/', function (req, res, done) {
  ChannelModel
    .paginate(
      {},
      {
        page: req.query.page,
        limit: req.query.limit,
        populate: ['user'],
        sort: {
          name: 'asc',
          created: 'desc'
        }
      },
      function (err, channel) {
        if (err) {
          res.status(500).json({
            object: 'error',
            data: {
              message: 'Error registering channel, check entries',
              code: 500
            },
            itemCount: 0,
            pageCount: 0
          });
        } else {
          res.status(200).json({
            object: 'list',
            data: channel.docs,
            itemCount: channel.total,
            pageCount: channel.pages
          });
        }
      }
    );
});

router.get('/:id', function (req, res, done) {
  ChannelModel
    .findOne(
      {
        _id: req.params.id
      },
      function (err, channel) {
        if (err || !channel) {
          res.status(404).json({
            object: 'error',
            data: {
              message: 'Error opening channel',
              code: 404
            },
            itemCount: 0,
            pageCount: 0
          });
        } else {
          res.status(200).json({
            object: 'object',
            data: channel,
            itemCount: 1,
            pageCount: 1
          });
        }
      }
    );
});

router.post('/', function (req, res, done) {
  ChannelModel
    .create(
      {
        name: striptags(req.body.name),
        user: req.session.user._id
      },
      function (err, channel) {
        if (err) {
          res.status(500).json({
            object: 'error',
            has_more: false,
            data: {
              message: 'Error registering channel, check entries',
              code: 500
            },
            itemCount: 0,
            pageCount: 0
          });
        } else {
          res.status(201).json({
            object: 'object',
            data: channel,
            itemCount: 1,
            pageCount: 1
          });
        }
      }
    );
});

module.exports = router;

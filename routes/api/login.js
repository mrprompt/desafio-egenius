'use strict';

var express = require('express');
var router = express.Router();
var striptags = require('striptags');
var bcrypt = require('bcrypt');
var salt = process.env.PASSWORD_SALT;
var UserModel = require('../../models/user');

router.post('/', function (req, res, done) {
  UserModel
    .findOne({
      email: striptags(req.body.email),
      password: bcrypt.hashSync(req.body.password, salt)
    })
    .exec(function (err, user) {
      if (err || !user) {
        res.status(403).json({
          object: 'object',
          has_more: false,
          data: {
            message: 'Invalid user or password',
            code: 403
          },
          itemCount: 0,
          pageCount: 0
        });
      } else {
        req.session.user = user;

        res.status(201).json({
          object: 'object',
          has_more: false,
          data: user,
          itemCount: 1,
          pageCount: 1
        });
      }
    });
});

module.exports = router;

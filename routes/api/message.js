'use strict';

var express = require('express');
var router = express.Router();
var striptags = require('striptags');
var MessageModel = require('../../models/message');

router.get('/:channel', function (req, res) {
  MessageModel
    .paginate(
      {
        channel: req.params.channel
      },
      {
        page: req.query.page,
        limit: req.query.limit,
        populate: ['user'],
        sort: {
          created: 'desc'
        }
      },
      function (err, message) {
        if (err) {
          res.status(500).json({
            object: 'error',
            data: {
              message: 'Error listening messages',
              code: 500
            },
            itemCount: 0,
            pageCount: 0
          });
        } else {
          res.status(200).json({
            object: 'list',
            data: message.docs,
            itemCount: message.total,
            pageCount: message.pages
          });
        }
      }
    );
});

router.post('/:channel', function (req, res) {
  MessageModel
    .create(
      {
        content: striptags(req.body.content),
        user: req.session.user._id,
        channel: req.params.channel
      },
      function (err, message) {
        if (err) {
          res.status(500).json({
            object: 'error',
            has_more: false,
            data: {
              message: 'Error registering message, check entries',
              code: 500
            },
            itemCount: 0,
            pageCount: 0
          });
        } else {
          res.status(201).json({
            object: 'object',
            data: message,
            itemCount: 1,
            pageCount: 1
          });
        }
      }
    );
});

module.exports = router;

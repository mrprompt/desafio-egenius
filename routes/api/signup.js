'use strict';

var express = require('express');
var router = express.Router();
var striptags = require('striptags');
var bcrypt = require('bcrypt');
var salt = process.env.PASSWORD_SALT;
var UserModel = require('../../models/user');

router.post('/', function (req, res, done) {
  UserModel
    .create(
      {
        name: striptags(req.body.name),
        email: striptags(req.body.email),
        password: bcrypt.hashSync(req.body.password, salt)
      },
      function (err, user) {
        if (err) {
          res.status(500).json({
            object: 'error',
            has_more: false,
            data: {
              message: 'Error registering user, check entries',
              code: 500
            },
            itemCount: 0,
            pageCount: 0
          });
        } else {
          res.status(201).json({
            object: 'object',
            has_more: false,
            data: user,
            itemCount: 1,
            pageCount: 1
          });
        }
      }
    );
});

module.exports = router;

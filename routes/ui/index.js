'use strict';

var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function (req, res) {
  if (req.session.user) {
    res.redirect('/channels/');
  }

  res.render('index', {title: 'Desafio eGenius - Entrar'});
});

module.exports = router;

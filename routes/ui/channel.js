'use strict';

var express = require('express');
var router = express.Router();

/* GET list of channels. */
router.get('/', function (req, res, next) {
  res.render('channels');
});

/* GET channel details. */
router.get('/:id', function (req, res, next) {
  res.render('channel', {channel : req.params.id});
});

module.exports = router;

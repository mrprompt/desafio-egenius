'use strict';

const PAGINATION = {
    MIN: 10,
    MAX: 1000
};

var express = require('express');
var paginate = require('express-paginate');
var session = require('express-session');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var connection = require('./modules/connection');
var checkLogin = require('./modules/session');

// start application
var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: false}));
app.use(cookieParser());
app.use(paginate.middleware(PAGINATION.MAX, PAGINATION.MAX));
app.use(session({secret: 'keyboard cat', resave: false, saveUninitialized: false}));
app.use(express.static(path.join(__dirname, 'public')));
app.use('/bower_components', express.static(path.join(__dirname, 'bower_components')));

// all routes obtain session
app.use(function(req, res, next) {
  res.locals.session = req.session;

  next();
});

// ui routers
app.use('/', require('./routes/ui/index'));
app.use('/signup/', require('./routes/ui/signup'));
app.use('/signout/', require('./routes/ui/signout'));
app.use('/channels/', checkLogin, require('./routes/ui/channel'));
app.use('/channel/', checkLogin, require('./routes/ui/channel'));

// api routers
app.use('/api/login/', require('./routes/api/login'));
app.use('/api/signup/', require('./routes/api/signup'));
app.use('/api/channel/', checkLogin, require('./routes/api/channel'));
app.use('/api/message/', checkLogin, require('./routes/api/message'));

// catch 404 and forward to error handler
app.use(function (req, res, next) {
    var err = new Error('Not Found');
    err.status = 404;
    next(err);
});

// development error handler
// will print stacktrace
if (app.get('node_env') === 'development') {
    app.use(function (err, req, res, next) {
        res.status(err.status || 500);
        res.render('error', {
            message: err.message,
            error: err
        });
    });
}

// production error handler
// no stacktraces leaked to user
app.use(function (err, req, res, next) {
    res.status(err.status || 500);

    res.render('error', {
        message: err.message,
        error: {}
    });
});

module.exports = app;

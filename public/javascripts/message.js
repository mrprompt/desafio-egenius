'use strict';

/**
 * Message Controller
 * @param $scope Scope Provider
 * @param $http Http Provider
 * @param $window Location Provider
 * @param $interval Interval Provider
 * @param $notify ngNotify
 * @constructor
 */
function MessageController($scope, $http, $window, $interval, $notify) {
  /**
   * Register message on api
   */
  $scope.createMessage = function (channel) {
    var message = $scope.message;

    $http
      .post('/api/message/' + channel, {
        content: message.content
      })
      .success(function (response) {
        delete $scope.message;
      })
      .error(function (error) {
        console.error(error);

        $notify.set(error.data.message, {type: 'error'});
      })
  };

  /**
   * Load messages from api
   */
  $scope.loadMessages = function (channel) {
    var $last = {
      _id: ''
    };

    $interval(function () {
      $http
        .get('/api/message/' + channel)
        .success(function (response, status, headers) {
          if (headers('ETag') !== $last._id) {
            $scope.messages = response.data;

            $last._id = headers('ETag');
          }
        })
        .error(function (error) {
          console.error(error);

          $notify.set(error.data.message, {type: 'error'});
        })
    }, 3000);
  };
}

angular
  .module('myApp.message', ['ngNotify', 'ui.gravatar'])

  .controller('MessageController', ['$scope', '$http', '$window', '$interval', 'ngNotify', MessageController]);

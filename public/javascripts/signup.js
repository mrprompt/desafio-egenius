'use strict';

/**
 *
 * @param $scope Scope Provider
 * @param $http Http Provider
 * @param $window Location Provider
 * @param $notify ngNotify
 * @constructor
 */
function SignupController($scope, $http, $window, $notify) {
  /**
   * Register user on api
   */
  $scope.signup = function () {
    var user = $scope.user;

    $http
      .post('/api/signup/', user)
      .success(function (response) {
        console.log('registered: ' + response.data);

        $notify.set('You are registered, welcome \\o\/! Now, we redirect you to login page, wait...', {type: 'success'});

        setTimeout(function () {
          $window.location.href = '/';
        }, 1000);
      })
      .error(function (error) {
        console.error(error);

        $notify.set(error.data.message, {type: 'error'});
      })
  }
}

angular
  .module('myApp.signup', ['ngNotify'])

  .controller('SignupController', ['$scope', '$http', '$window', 'ngNotify', SignupController]);

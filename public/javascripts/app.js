'use strict';

angular
  .module('myApp', [
    'angular-loading-bar',
    'myApp.home',
    'myApp.signup',
    'myApp.channel',
    'myApp.message'
  ]);

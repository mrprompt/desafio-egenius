'use strict';

/**
 * Channel Controller
 * @param $scope Scope Provider
 * @param $http Http Provider
 * @param $window Location Provider
 * @param $interval Interval Provider
 * @param $notify ngNotify
 * @constructor
 */
function ChannelController($scope, $http, $window, $interval, $notify) {
  /**
   * Register channel on api
   */
  $scope.createChannel = function () {
    var channel = $scope.channel;

    $http
      .post('/api/channel/', channel)
      .success(function (response) {
        console.log('registered: ' + response.data);

        $notify.set('Channel created', {type: 'success'});

        $('#myModal').modal('hide');

        setTimeout(function () {
          $window.location.href = '/channel/' + response.data._id;
        }, 500);
      })
      .error(function (error) {
        console.error(error);

        $notify.set(error.data.message, {type: 'error'});
      })
  };

  /**
   * Load channels from api
   */
  $scope.loadChannels = function () {
    var $last = {
      _id: ''
    };

    $interval(function () {
      $http
        .get('/api/channel/')
        .success(function (response, status, headers) {
          if (headers('ETag') !== $last._id) {
            $scope.channels = response.data;

            $last._id = headers('ETag');
          }
        })
        .error(function (error) {
          $notify.set(error.data.message, {type: 'error'});
        })
    }, 5000);
  };

  /**
   * Load channel from api
   */
  $scope.loadChannel = function (channel) {
    $http
      .get('/api/channel/' + channel)
      .success(function (response) {
        $scope.channel = response.data;
      })
      .error(function (error) {
        console.error(error);

        $notify.set(error.data.message, {type: 'error'});
      })
  };
}

angular
  .module('myApp.channel', ['ngNotify'])

  .controller('ChannelController', ['$scope', '$http', '$window', '$interval', 'ngNotify', ChannelController]);

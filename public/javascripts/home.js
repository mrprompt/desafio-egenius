'use strict';

/**
 *
 * @param $scope Scope Provider
 * @param $http Http Provider
 * @param $window Location Provider
 * @param $notify ngNotify
 * @constructor
 */
function HomeController($scope, $http, $window, $notify) {
  /**
   * Authenticate user on api and redirect to channels list
   */
  $scope.login = function () {
    var user = $scope.user;

    $http
      .post('/api/login/', user)
      .success(function (response) {
        console.log('logged in: ' + response.data);

        $notify.set('Welcome :)', {type: 'info'});

        setTimeout(function () {
          $window.location.href = '/channels';
        }, 1000);
      })
      .error(function (error) {
        console.error(error);

        $notify.set(error.data.message, {type: 'error'});
      })
  }
}

angular
  .module('myApp.home', ['ngNotify'])

  .controller('HomeController', ['$scope', '$http', '$window', 'ngNotify', HomeController]);

'use strict';

var router = require('express').Router();
var session = require('express-session');

router.all('*', function(req, res, next) {
    if (req.method === 'OPTIONS') {
        next();

        return true;
    }

    if (!req.session.user) {
        return res.redirect('/');
    }

    req.app.locals.user = req.session.user;

    next();
});

module.exports = router;
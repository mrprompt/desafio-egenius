'use strict';

var dbUser = process.env.DB_USERNAME  || 'admin';
var dbPass = process.env.DB_PASSWORD  || 'admin';
var dbHost = process.env.DB_HOST      || '127.0.0.1';
var dbPort = process.env.DB_PORT      || '27017';
var dbName = process.env.DB_NAME      || 'test';
var dbUri  = dbUser + ":" + dbPass + "@" + dbHost + ':' + dbPort + '/' + dbName;

var mongoose = require('mongoose');
    mongoose.connect(dbUri);

// When successfully connected
mongoose.connection.on('connected', function () {
    console.log('Mongoose default connection open to ' + dbUri);
});

// If the connection throws an error
mongoose.connection.on('error',function (err) {
    console.log('Mongoose default connection error: ' + err);
});

// When the connection is disconnected
mongoose.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

// If the Node process ends, close the Mongoose connection
process.on('SIGINT', function() {
    mongoose.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');

        process.exit(0);
    });
});


exports.mongoose = mongoose;

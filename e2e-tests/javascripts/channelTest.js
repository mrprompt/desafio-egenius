'use strict';

describe('myApp.channel module', function () {
  beforeEach(module('myApp.channel'));

  describe('Channel controller', function () {
    it('should ....', inject(function ($controller) {
      var dependencies = {
        $scope: {},
        $http: function () {

        },
        $window: function () {

        },
        $notify: function () {

        }
      };

      var channelController = $controller('ChannelController', dependencies);

      expect(channelController).toBeDefined();
    }));
  });
});

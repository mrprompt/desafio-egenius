'use strict';

describe('myApp.signup module', function () {
  beforeEach(module('myApp.signup'));

  describe('Signup controller', function () {
    it('should ....', inject(function ($controller) {
      var dependencies = {
        $scope: {},
        $http: function () {

        },
        $window: function () {

        },
        $notify: function () {

        }
      };

      var signupController = $controller('SignupController', dependencies);

      expect(signupController).toBeDefined();
    }));
  });
});

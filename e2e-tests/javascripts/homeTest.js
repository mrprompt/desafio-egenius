'use strict';

describe('myApp.home module', function () {
  beforeEach(module('myApp.home'));

  describe('Home controller', function () {
    it('should ....', inject(function ($controller) {
      var dependencies = {
        $scope: {},
        $http: function () {

        },
        $window: function () {

        },
        $notify: function () {

        }
      };

      var homeController = $controller('HomeController', dependencies);

      expect(homeController).toBeDefined();
    }));
  });
});

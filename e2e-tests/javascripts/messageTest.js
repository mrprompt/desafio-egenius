'use strict';

describe('myApp.message module', function () {
  beforeEach(module('myApp.message'));

  describe('Message controller', function () {
    it('should ....', inject(function ($controller) {
      var dependencies = {
        $scope: {},
        $http: function () {

        },
        $window: function () {

        },
        $notify: function () {

        }
      };

      var messageController = $controller('MessageController', dependencies);

      expect(messageController).toBeDefined();
    }));
  });
});

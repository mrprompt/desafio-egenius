exports.config = {
    specs: [
        './scenarios.js'
    ],

    capabilities: {
        browserName: 'phantomjs',
        maxInstances: 1
    },

    baseUrl: 'http://localhost:3000/',

    framework: 'jasmine',

    seleniumAddress: 'http://localhost:4444/wd/hub',

    jasmineNodeOpts: {
        defaultTimeoutInterval: 30000
    }
};

'use strict';

describe('my app', function () {
    describe('home', function () {
        beforeEach(function () {
            browser.get('/');
        });

        it('should render home when user navigates to /', function () {
            expect(element.all(by.css('.form-signin')).first().getText()).toMatch(/.?/);
        });
    });

    describe('signup', function () {
        beforeEach(function () {
            browser.get('/signup/');
        });

        it('should render home when user navigates to /signup/', function () {
            expect(element.all(by.css('.form-signup')).first().getText()).toMatch(/.?/);
        });
    });

    describe('channels', function () {
        beforeEach(function () {
            browser.get('/channels/');
        });

        it('should render home when user navigates to /channels/', function () {
          expect(browser.getLocationAbsUrl()).toMatch("");
        });
    });

    describe('channels', function () {
        beforeEach(function () {
            browser.get('/channel/1');
        });

        it('should render home when user navigates to /channel/1', function () {
          expect(browser.getLocationAbsUrl()).toMatch("");
        });
    });

    describe('signout', function () {
        beforeEach(function () {
            browser.get('/signout/');
        });

        it('should redirect to / when navigates to /signout/', function () {
            expect(browser.getLocationAbsUrl()).toMatch("");
        });
    });
});

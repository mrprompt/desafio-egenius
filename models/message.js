'use strict';

var mongoose = require('mongoose'),
  MessageSchema = new mongoose.Schema({
    content: {
      type: String,
      required: true
    },
    channel: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'Channel'
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    created: {
      type: Date,
      default: Date.now
    }
  })
    .plugin(require('mongoose-paginate'));

module.exports = mongoose.model('Message', MessageSchema);

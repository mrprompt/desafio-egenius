'use strict';

var mongoose = require('mongoose'),
  ChannelSchema = new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    user: {
      type: mongoose.Schema.Types.ObjectId,
      ref: 'User'
    },
    created: {
      type: Date,
      default: Date.now
    }
  })
    .plugin(require('mongoose-paginate'));

module.exports = mongoose.model('Channel', ChannelSchema);

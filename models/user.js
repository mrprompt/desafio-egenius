'use strict';

var mongoose = require('mongoose'),
  UserSchema = new mongoose.Schema({
    name: {
      type: String,
      required: true
    },
    email: {
      type: String,
      index: true,
      required: true,
      unique: true
    },
    password: {
      type: String,
      required: true,
      bcrypt: true
    },
    created: {
      type: Date,
      default: Date.now
    }
  })
    .plugin(require('mongoose-paginate'))
    .plugin(require('mongoose-unique-validator'))
    .set('toJSON', {
      transform: function (doc, ret, options) {
        delete ret.password;

        return ret;
      }
    });

module.exports = mongoose.model('User', UserSchema);
